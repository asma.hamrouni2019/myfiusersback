var express = require('express');
var router = express.Router();
const db = require('../database/models/index')

/* GET users listing. */
router.post('/', function (req, res, next) {

    db.users.build(req.body)
        .save()
        .then(user => {
            return res.send({
                'success': true,
                'message': 'user created successfully'
            });
        })
        .catch(error => {
            return res.status(500).send({
                'success': false
            });
        })


});

module.exports = router;
