/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('users', {
    index: {
      type: DataTypes.BIGINT,
      autoIncrement: true,
      primaryKey: true
    },
    birthDay: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    birthPlace: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    city: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    clientId: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    country: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    creatorPersonname: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    dateidcard: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    district: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    email: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    fatherName: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    firstName: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    formCreatetime: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    gender: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    gsmNumber: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    idcard: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    idcardpicture: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    kindofService: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    lastName: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    latitude: {
      type: DataTypes.DOUBLE,
      allowNull: true
    },
    longitude: {
      type: DataTypes.DOUBLE,
      allowNull: true
    },
    motherName: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    neighborhood: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    number: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    page: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    passport: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    password: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    phoneNumber: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    place: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    postCode: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    serienumber: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    servicePacket: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    staticIp: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    stationinfoName: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    street: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    streetNumber: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    taxid: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    tc: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    userName: {
      type: DataTypes.TEXT,
      allowNull: true
    }
  }, {
    tableName: 'users',
    timestamps: false
  });
};
